## gRPC with Java and manual compilation without Maven or Gradle with JARs for protoc v3.21.2 and gRPC protoc-plugin 1.47.0

All instructions for Linux: should be the same for Mac OS X, but Windows needs adjustments!

The `protoc` compiler is needed that also calls the gRPC protoc-plugin. 
Both are developed independently from each other and have different version, e.g.
protoc is version 3.21.2 (sometimes the first digit is omitted, i.e. 21.2) (file for 64 bit Linux: protoc-21.2-linux-x86_64.zip )
and the gRPC protoc-plugin 1.47.0 (file for 64 bit Linux: protoc-gen-grpc-java-1.47.0-linux-x86_64.exe ). Also the JARs used as dependency need to have a matching version. As gRPC and Protocol Buffers are developed independently from each other, it is not trivial to find versions that work together. 

**Currently, this Git repository has JARs matching protoc v3.21.2 and gRPC protoc-plugin 1.47.0, so take care to switch to these versions, when doing the downloads below.** 

1. Download protoc from https://github.com/protocolbuffers/protobuf/releases , i.e. scroll to `Assets`, click there on `Show all 28 assets` (the number may vary), and download there the 64 Bit version of the `protoc` zip file for your operating system (note: nowadays all systems use 64 bit) and CPU, e.g. `protoc-21.2-linux-x86_64.zip` 

   To find older versions (e.g. v21.2), go to the https://github.com/protocolbuffers/protobuf/releases page and use there the `Find a release` search box and type-in there the version number, e.g. v21.2., and scroll until you find that release. Expand `Assets` to see all files of that release.
   Unzip the archive.

   If you want to automate downloading that version, e.g., for a Linux system use:
   ```
   wget https://github.com/protocolbuffers/protobuf/releases/download/v21.2/protoc-21.2-linux-x86_64.zip
   ```
   ```
   unzip protoc-21.2-linux-x86_64.zip
   ```

2. Download the gRPC protoc-plugin from Maven central https://search.maven.org/search?q=g:io.grpc%20AND%20a:protoc-gen-grpc-java , i.e. click on the very right on download (choose the `_64.exe` file specific to your OS), e.g. `protoc-gen-grpc-java-1.47.0-linux-x86_64.exe` 
Important: on Linux and Mac OS, you need to do a `chmod a+x` on it after downloading in order to make it executable! 

   To find older versions (e.g. v1.47.0 that matches the protoc v21.2), go to the https://search.maven.org/search?q=g:io.grpc%20AND%20a:protoc-gen-grpc-java page and have there a look at the column `Latest version`: next to the latest version number you should see there in brackets `(99+)`, click on that to get a clickable list of all version. Click there on, e.g. 1.47.0, and then on the top right on `Download` and choose the `.exe` that matches your operating system (note: nowadays all systems use 64 bit) and CPU, e.g. `linux-x86_64.exe` 

   If you want to automate downloading that version, e.g., for a Linux system use: 
   ```
   wget -O protoc-gen-grpc-java-1.47.0-linux-x86_64.exe https://search.maven.org/remotecontent?filepath=io/grpc/protoc-gen-grpc-java/1.47.0/protoc-gen-grpc-java-1.47.0-linux-x86_64.exe
   ```
   ```
   chmod a+x protoc-gen-grpc-java-1.47.0-linux-x86_64.exe
   ```

3. Do a 
   ```
   git clone https://gitlab.com/helmut.neukirchen/grpcManualJava.git
   ```
    and then move the above tools into the project directory (only the `.exe` files are needed, i.e. no need to copy, e.g., the `include` directory.)

   I assume that `protoc` and `protoc-gen-grpc-java` are within this project's root directory, i.e. you have to copy them into `grpcManualJava`:
   ```
   cp bin/protoc grpcManualJava/
   ```
   ```
   cp protoc-gen-grpc-java-1.47.0-linux-x86_64.exe grpcManualJava/
   ```

   And then change into the project directory:
   ```
   cd grpcManualJava
   ```

4. Generate protobuf and GRPC Java source code from the `.proto` file into the directory `src/main/generated` by a something like this (assuming that you are in the root directoy of your project and that root directory contains also `protoc` and `protoc-gen-grpc-java` (add version number and OS to that file name). Note that on MS Windows systems, the slashes ("`/`") in pathnames need to be replaced by backslashes ("`\`"): 
`protoc --plugin=protoc-gen-grpc-java=protoc-gen-grpc-java-1.40.1-linux-x86_64.exe --proto_path=src/main/proto --java_out=src/main/generated --grpc-java_out=src/main/generated src/main/proto/HelloService.proto` 

   but you need to either have `protoc` in your `PATH` or call it via its full absolute pathname and also the the full absolute pathname for the plugin needs to be correct. For example, if the project root directory is `/home/azureuser/grpcManualJava/`, then **for running without setting `PATH`**, I need to add that pathname prefix (again, on MS Windows systems, the slashes ("`/`") in pathnames need to be replaced by backslashes ("`\`"):

   ``` 
   /home/azureuser/grpcManualJava/protoc --plugin=protoc-gen-grpc-java=/home/azureuser/grpcManualJava/protoc-gen-grpc-java-1.47.0-linux-x86_64.exe --proto_path=src/main/proto --java_out=src/main/generated --grpc-java_out=src/main/generated src/main/proto/HelloService.proto
   ``` 

   Files should then have been generated in `src/main/generated/com` (if you want, you could also specify to have seperate directories for the protobuf and GRPC generated code -- but for simplicity, the same directory is used here).


   All the needed dependencies are in directory `jars`. (I have created them from a Maven-based project where I ran and do the 
`mvn dependency:copy-dependencies` to download all needed jars into `target/dependency` and copied them then into directory `jars`.)
Note that I have also added a fat jar that contains all in one jar: `GrpcAllInOne.jar` -- you need either that fat jar only or all the other jars.

5. Next compile the code. Note that we have two source folders: the one with the generated code and the one with our code and in addition three directories with further source files that need to be compiled (Note that on MS Windows systems, the colon (`:`) that seperates paths (e.g. in `-cp` or `-sourcepath` parameter) needs to be replaced by a semicolon (`;`), and again the slashes ("`/`") in pathnames need to be replaced by backslashes ("`\`")):

   ``` 
   javac -cp "jars/*" -d classes/ -sourcepath src/main/java/:src/main/generated/ src/main/generated/com/baeldung/grpc/*java src/main/java/com/baeldung/grpc/client/*.java src/main/java/com/baeldung/grpc/server/*.java
   ``` 

   If you get compile errors, such as `error: cannot find symbol`, then this is most likely because the protoc and protoc-gen-grpc-java versions do not match the JAR versions that are used, i.e. code is generated using some API that is not there in the JARs.

6. If we have all JARs in directory `jars` (should be the case when you did `git clone` this prject), we can start the server via (again, on MS Windows systems, the colon (`:`) that seperates paths (in the `-cp`) needs to be replaced by a semicolon (`;`), and again the slashes ("`/`") in pathnames need to be replaced by backslashes ("`\`")): 
   ``` 
   java -cp "classes:jars/*" com.baeldung.grpc.server.GrpcServer
   ```  
   and start the client via (without command line parameters, hostname "localhost" and portnumber "8080" are used):
   ``` 
   java -cp "classes:jars/*" com.baeldung.grpc.client.GrpcClient
   ``` 

   or provide via command line parameter portnumber for the server, e.g.
   ``` 
   java -cp "classes:jars/*" com.baeldung.grpc.server.GrpcServer 8080
   ```  
   and hostname and postnumber for the client
   ``` 
   java -cp "classes:jars/*" com.baeldung.grpc.client.GrpcClient 127.0.0.1 8080
   ``` 


## With Maven

See https://gitlab.com/helmut.neukirchen/grpcbaelding


## Some notes on the modifications made to the example from Baeldung

The source code here is based on [https://www.baeldung.com/grpc-introduction](https://www.baeldung.com/grpc-introduction) -- Note that the source code snippets on that web page do not contain, e.g., `import` statements. The full code from Baeldung can be found in GitHub, in the following sub-directory (Git does not allow to check-out only a sub-directory, so you need to have checked-out the whole project and only use that sub-directory): [https://github.com/eugenp/tutorials/tree/master/grpc](https://github.com/eugenp/tutorials/tree/master/grpc) -- Note that this then, however, contains more files than the simple tutorial from the above web page and these extra files were deleted here in `src`. 
