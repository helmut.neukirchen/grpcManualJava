package com.baeldung.grpc.server;

import java.io.IOException;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class GrpcServer {

    // Call without command line parameter: port 8080 is used,
    // One parameter sets the portnumber

    public static void main(String[] args) throws IOException, InterruptedException {
        int portnumber=8080;

        switch (args.length) {
             case 1: portnumber=Integer.parseInt(args[0]);
        }
        System.out.println("Using port number: "+portnumber);

        Server server = ServerBuilder.forPort(portnumber)
          .addService(new HelloServiceImpl()).build();

        System.out.println("Starting server...");
        server.start();
        System.out.println("Server started!");
        server.awaitTermination();
    }
}
