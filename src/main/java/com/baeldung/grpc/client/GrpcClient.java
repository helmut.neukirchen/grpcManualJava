package com.baeldung.grpc.client;

import com.baeldung.grpc.HelloRequest;
import com.baeldung.grpc.HelloResponse;
import com.baeldung.grpc.HelloServiceGrpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

 
public class GrpcClient {

    // Call without command line parameter: localhost:8080 is used,
    // One parameter sets the hostname (keeping 8080 as port number)
    // A second parameter sets the portnumber

    public static void main(String[] args) throws InterruptedException {
        String hostname="localhost";
        int portnumber=8080;

        switch (args.length) {
             case 2: portnumber=Integer.parseInt(args[1]);
             case 1: hostname=args[0];
        }

        System.out.println("Using host name: "+hostname);
        System.out.println("Using port number: "+portnumber);

        ManagedChannel channel = ManagedChannelBuilder.forAddress(hostname, portnumber)
            .usePlaintext()
            .build();

        HelloServiceGrpc.HelloServiceBlockingStub stub 
          = HelloServiceGrpc.newBlockingStub(channel);

        HelloResponse helloResponse = stub.hello(HelloRequest.newBuilder()
            .setFirstName("Baeldung")
            .setLastName("gRPC")
            .build());

        System.out.println("Response received from server:\n" + helloResponse);

        channel.shutdown();
    }
}
